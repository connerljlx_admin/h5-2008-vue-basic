回顾：
```
new Vue({
  el:'#app',
  data: {

  },
  methods: {

  }
})

{{}}

// 渲染数据  调用方法
// 变量 认识vm上 全局变量 （白名单）
v-model
v-text
v-html
v-bind:属性  :bind
v-on:事件 @事件

v-show 
v-if

v-else-if

v-else

循环v-for
<li v-for="item in arr">
<li v-for="(item,index) in arr">
```
## 深入 vue mvvm原理
Vue利用Object.defineProperty()

过程：
  当我们 new Vue 创建一个vm(实例时),需要传入一个对象，对象有一个data属性也是对象，vue会自动遍历 data对象。拿到他里面所有属性，使用Object.defineProperty;得到每一个data中属性的getter和setter
  每一个 实例创建后 还会创建一个 watcher(观察者), 在setter触发时（data中属性值改变了），通知观察者，
  观察者回调函数中，会立即调用（render渲染模板 一般是隐藏），实例的render方法，重新生成 虚拟dom ...

  虚拟dom在vue中的 作用：
    第一次 vue在渲染真实dom后，都会将基于真实dom，生成虚拟dom，保存在内存中，当改变了数据后，setter通知观察者，重新调用render函数 生成 数据改变 新的虚拟的dom树，和 上一次 保存在内存中的虚拟dom树进行比较(diff算法) ，得到最少代价去操作dom
  diff在比较中做了什么：
    dom树中 不同的层级  按照相同层级进行比较
  key属性在diff算法 比较时做了什么：
  要求key是独一无二
  要求：循环中 必须要加 key属性  为了加快diff 比较效率


  虚拟dom
  概念：用js 对象的 结构来描述一段dom树
  ```js
    <div id="box" class="wrap">
      <p class="op">你是p</p>
      <span>我是span</span>
      文本内容
    </div>
    {
      tagName:"div",
      attrs:{
        id: 'box',
        className:"wrap"
      },
      children: [
        {
          tagName:"p",
          attrs: {
            className:'op'
          },
          chidren: ['你是p']
        },
        {
          tagName:"span",
          attr:null,
          children: ['我的span']
        },
        '文本内容'
      ]
    }

  ```
## vue中的 class 
:class 
```
<div :class="a"></div> // class="box"
<div :class="b">  // class="box box2"
<div :class="[a,'box2']"> // class="box box2"
<div :class="{active:表达式}"> // div有没有active类，取决于active属性值是不是真


{
  data:{
    a: 'box',
    b: [a,'box2']
  }
}
```
## vue中的style
将 元素的style 映射成 对象的结构 
好处：样式值 就可以写表达式（值、变量、计算、三目、短路）
```
<div :style="{ width: 200+300+'px', height: 1>2?'300px':'500px',background:'red' }">
```

## vue开发方式 是 数据驱动 

## mvvm一些 bug