const path = require('path')
module.exports = {
  devServer: {
    port: 9527,
    open: true
  },
  lintOnSave: false,
  chainWebpack: (config) => {
    config.resolve.alias
      .set('@', path.join(__dirname, 'src'))
      .set('@views', path.join(__dirname, 'src/views'))
      .set('@components', path.join(__dirname, 'src/components'))
  }
}
