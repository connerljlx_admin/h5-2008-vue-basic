import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
// 创建vuex store 仓库对象
const store = new Vuex.Store({
  state: {
    num: 10,
    arr: [1, 2, 3, 4]
  },
  getters: {
    /*
    相当于vuex中计算属性，依赖 store state派生新的状态
    注意：不要修改getter 应该修改state 当state变化时，getter自动重新计算
    */
    doubleNum: (state) => {
      return state.num * 2
    }
  },
  mutations: {
    addNum (state, n) {
      state.num += n
    },
    reduceNum (state, n) {
      state.num -= n
    }
  },
  actions: {
    /* addNumAsync (context, params) {
      console.log(context, 111)
      // context 就是 store这个对象
      setTimeout(() => {
        context.commit('addNum', params)
      }, 2000)
    } */
    addNumAsync ({ commit }, params) {
      setTimeout(() => {
        commit('addNum', params)
      }, 2000)
    }
  }
})
export default store
