export default {
  addNumAsync ({ commit }, params) {
    setTimeout(() => {
      commit('addNum', params)
    }, 2000)
  }
}
