export default {
  addNum (state, n) {
    state.num += n
  },
  reduceNum (state, n) {
    state.num -= n
  }
}
