export default {
  namespaced: true,
  state: () => ({
    num: 10
  }),
  mutations: {
    addNum (state, n) {
      console.log('cate 执行')
      state.num += n
    },
    reduceNum (state, n) {
      state.num -= n
    }
  },
  actions: {
    addNumAsync ({ commit }, n) {
      setTimeout(() => {
        commit('addNum', n)
      }, 2000)
    }
  }
}
