import Vue from 'vue'
import Vuex from 'vuex'
import item from './item'
import cate from './cate'
Vue.use(Vuex)
const store = new Vuex.Store({
  modules: {
    item,
    cate
  }
})

export default store
