### vuecli使用
vue create 项目
脚本命令
```js
npm run serve // 启动开发环境服务器 开发环境代码运行服务器
npm run build // 构建生产环境代码
npm run lint // 根据项目eslint配置 自动格式化代码
```
### 分析目录结构
node_modules 当前项目依赖包
public 静态资源目录
  index.html  单页面应用的 index.html
  favicon 页面 标题图标
src 源码目录
  assets 静态资源目录  项目中使用的静态资源 推荐在这里
  components 存在 应用 的 公共 组件
  router 路由配置文件
  store vuex目录
  views 路由组件目录
  App.vue 根组件 （不是路由组件，默认直接渲染）router-view
  main.js
    入口文件 运行时（build），自动引入到index.html上
    所有 在main.js中运行的代码，直接运行index.html上，所以main.js中配置都是全局
    *** main.js中引入的资源（css），影响全局（全部路由组件）
  .browserslistrc  // post-css自动给css3 加浏览器前缀
  babel.config.js //  babel配置规则  es6转es5
## es6中的模块化
注意注意：
  当前vueCli（webpack）搭建环境支持 node和es6模块化
注意注意注意：
  webpack(当前脚手架启动项目)，支持任意类型文件引入
  比如 css 比如 图片 比如字体图标（在js中）

import 必须在顶部引入

+  直接导入一个文件 （没有导出接口文件）

如果一个文件（在当前webpack环境可以是非js的如 css 图片 字体图标） 没有导出模块 那么这个文件就是一个模块
可以直接引入这个模块 相当于 这个文件代码在引入地方运行
```js
import '路径'
```
+ 一个文件导出多个接口 （只能是js）
```js
// a.js
export const a = 10
export const c = 20
export const fn = () => {
  console.log(111)
}
// 导出多个接口第二种方式
const a = 10
const c = 20
const fn = () => {

}

export { // 注意 这不是es6对象简写 固定语法
  a,
  c,
  fn
}


// b.js 引入
import { a, c } from '路径' // 按需引入
import * as 别名 from '路径' // 全部引入 别名就是一个变量 对象
```
+ 一个文件导出一个接口
```js
export default 值
// 引入
import 变量 from 'xxx'
```

注意：
  模块化 引入 第三方模块 （node_modules）中的模块
  直接写 模块名
  如果自定义模块 一定要加 路径 哪怕的同一目录 也要加 ./

### vue中的单文件组件
依赖 webpack vue-loader
.vue结尾文件 webpack自动解析成 组件对象
```vue
<template>
  <div> 
      xxxx
  </div>
</template>
<script>

export default {
  data () {
    return {

    }
  },
  methods: {},
  watch: {},
  components: {

  }
}
</script>
<style lang="scss" scoped>
  /* 
  lang属性指定当前 style 可以使用 预处理器 不加就是纯css
  scoped 属性 指定 当前样式只针对当前组件生效
  当前组件样式 
  /deep/ 深度选择器 穿透scoped 修改子组件样式
  */
</style>
```
## 路由懒加载
路由组件 在 渲染其他 路由时，不会引入
在当前路由组件对应路由匹配才引入这个组件js
好处：提高首屏打开速度
```js
{
  path: '/about',
  component: () => import('@/views/About')
}
```
## 自定cli配置
在项目根目录下创建vue.config.js
注意 修改配置后 需要重启生效
```js
const path = require('path')
module.exports = {
  devServer: { // 服务器
    port: 9527,
    open: true
  },
  lintOnSave: false, // 保存 lint 不检查代码格式
  chainWebpack: (config) => { // 自定义webpack配置
    config.resolve.alias // 自定义路径别名
      .set('@', path.join(__dirname, 'src'))
      .set('@views', path.join(__dirname, 'src/views'))
      .set('@components', path.join(__dirname, 'src/components'))
  }
}
```
## vuex
vue 一个状态管理插件
是vue 状态管理插件,采用集中式方式 管理 状态。且保证修改状态只能通过内部 mutation来改变 保证状态是可追踪的

使用插件
Vue.use(插件[,param])
```js
// src store index.js
import Vue from 'vue'
import Vuex from 'vuex'
Vue.use(Vuex)
// 创建仓库
const store = Vuex.Store({
  state: {
    num: 10 // 保存状态 数据
  },
  mutations: { // 保存修改状态的方法
    addNum (state, params) { // params调用mutation传的参数
      state.num += params
    }
  },
  actions: {
    /* 
    不能直接修改state。只有mutation可以直接修改state,action触发时可以提交mutaion
    action内部可以 写任意的异步代码
    什么用：
    如果一个状态的初始值在vuex的state中管理，且值需要 请求一个接口才能得到
    那么 就应该 创建一个action 在action中发请求。得到结果后，提交一个mutation将结果传入。调用mutation修改state,定义初始值

    action第一个参数context 就是store这个对象
    */
   setNumAsync (context, params) {
     fetchData(params).then(res => {
       if (res.data.code === 200) {
         context.commit('setNum', res.data.data)
       }
     })
   }
  }
})
// 组件中使用 state
this.$store.state.xxx
// 组件中提交mutation
this.$store.commit('mutation名字'[,param])
// 组件中 触发action
this.$store.dispatch('action名字'[,param])
```
## vuex提供的助手函数
+ mapState
```js 
import { mapState } from 'vuex'

mapState(['num', 'num2'])
// 调用后的结果是
{
   num () {
     return store.state.num
   },
   num2 () {
     return store.state.num
   }
}
/* 
注意：
  如果store中一个 数据 是 引用类型，最好不要用mapState
  重新定义一个计算属性，且深克隆他的值
  {
    arr(){
      return JSON.parse(JSON.stringify(this.$store.state.arr))
    }
  }
*/
```
+ mapMutations

```js
import { mapMutations } from 'vuex'

mapMutations(['addNum', 'reduceNum'])
// 结果
{
  addNum (params) {
    store.commit('addNum', params)
  },
  reduceNum (params) {
    store.commit('reduceNum', params)
  }
}


{
  methods: {
    ...mapMutations(['addNum', 'reduceNum'])
  }
  // 新建两个同名方法，调用这个方法即可 commit这个mutation
}
```
+ mapActions
```js
import { mapActions } from 'vuex'

mapAction(['addNumAsync'])
// 结果
{
  addNumAsync (params) {
    store.dispatch('addNum', params)
  }
}


{
  methods: {
    ...mapActions(['addNumAsync'])
  }
  // 新建两个同名方法，调用这个方法即可 commit这个mutation
}
```
## getters
需求：
  有时候我们 需要基于 已有的state 派生出 新的state通过getter来实现
  类似于 组件中的计算属性（基于已有的data派生新的数据）
```js
const store = new Vuex.Store({
  state: {
    num: 10
  },
  getters: {
    doubleNum (state) {
      return state.num*2
    }
  }
})
// 组件中使用
this.$store.getters.xxx
// 通过mapGetters这个助手函数
import { mapGetters } from 'vuex'
{
  computed: {
    ...mapGetters(['doubleNum'])
  }
}
// 组件中创建计算属性doubleNum 直接使用这个计算属性即可
```
## vuex中modules
当state 分层 
将 state mutations actions 分层
便于进一步管理和维护
```js
// 模块1
const moduleA = {
  state: () => ({ 
    num: 10
   }),
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}
// 模块2
const moduleB = {
  state: () => ({ ... }),
  mutations: { ... },
  actions: { ... }
}

const store = new Vuex.Store({
  modules: {
    a: moduleA,
    b: moduleB
  }
})
// 组件中获取 state
// 1
this.$store.state.模块名.状态名
// eg
this.$store.state.item.num
// 通过助手函数
{
  computed: {
    ...mapState({
      num: state => state.模块名.xx
    })
  }
}
注意：如果一个模块没有加命名空间 那么 所有mutations和actions是同级
当组件中触发 一个mutation 或者action 会触发所有模块中同名的mutation 或者action

怎解解决
  给每一个模块加命名空间属性

const moduleA = {
  namespaced: true,
  state: () => ({ 
    num: 10
   }),
  mutations: { ... },
  actions: { ... },
  getters: { ... }
}
// 组件提交mutation  action一样
this.$store.commit('模块名/mutation名字'[,param])
eg
this.$store.commit('item/addNum', 10)
// 通过助手函数
{
  ...mapMutations(['item/addNum'])
}
this['item/addNum'](param)
// 助手函数第二种方式
{
  ...mapMutations('模块名', ['mutation名字']),
  ...mapMutations('item', ['addNum']) // eg
}
this.addNum(param)

```
## vue常见UI组件库
+ pc端  （后台管理）
element-ui  饿了么
iview 
ant-design-vue   *** 蚂蚁金服
+ 移动端
mint-ui 饿了么
vant-ui 有赞
nut-ui  京东
cube-ui 滴滴